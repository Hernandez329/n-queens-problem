from pprint import pprint

def is_safe(board, row, col):
    
    for i in range(col):
        if board[row][i] == 1:
            return False

    for i, j in zip(range(row, -1, -1), range(col, -1, -1)):
        if board[i][j] == 1:
            return False

    for i, j in zip(range(row, 8, 1), range(col, -1, -1)):
        if board[i][j] == 1:
            return False

    return True

def place_queens(board, col):
    if col >= 6:
        return True

    for row_idx in range(8):
        if is_safe(board, row_idx, col):
            board[row_idx][col] = 1

            if place_queens(board, col + 1):
                return True

            board[row_idx][col] = 0

    return False

def chessboard():
    board = [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]
    ]

    place_queens(board, 0)

    return board

if __name__ == '__main__':
    board = chessboard()
    pprint(board, width=100)